
<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
 session_start();
 require_once("/config/config.php");
 require_once("logout.php");
 logout();
 $id_movie = mysql_real_escape_string($_GET['id_movie']);
 $sql2 = mysql_query("SELECT * FROM movie, kategori WHERE movie.id_kategori = kategori.id_kategori AND movie.id_movie = '$id_movie'");
 $data2 = mysql_fetch_array($sql2);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Booking Tiket Bioskop</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Booking tiket film, booking tiket murah, film indonesia" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/medile.css" rel='stylesheet' type='text/css' />
<link href="css/single.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/contactstyle.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/faqstyle.css" type="text/css" media="all" />
<!-- news-css -->
<link rel="stylesheet" href="news-css/news.css" type="text/css" media="all" />
<!-- //news-css -->
<!-- list-css -->
<link rel="stylesheet" href="list-css/list.css" type="text/css" media="all" />
<!-- //list-css -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="css/font-awesome.min.css" />
<!-- //font-awesome icons -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
<script src="js/owl.carousel.js"></script>
<script>
	$(document).ready(function() { 
		$("#owl-demo").owlCarousel({
	 
		  autoPlay: 3000, //Set AutoPlay to 3 seconds
	 
		  items : 5,
		  itemsDesktop : [640,5],
		  itemsDesktopSmall : [414,4]
	 
		});
	 
	}); 
</script> 
<script src="js/simplePlayer.js"></script>
<script>
	$("document").ready(function() {
		$("#video").simplePlayer();
	});
</script>

</head>
	
<body>
<!-- header -->
	<div class="header">
		<div class="container">
			<div class="w3layouts_logo">
				<a href="index.php"><img src="images/logo.png" wdith="150"></a>
			</div>
			<div class="w3_search">
				<form action="search.php" method="GET">
					<input type="text" name="search" placeholder="Search" required="">
					<input type="submit" value="Go">
				</form>
			</div>
			<div class="w3l_sign_in_register">
				<ul>
					<li><i class="fa fa-phone" aria-hidden="true"></i> (+62) 822 4259 8711</li>
					<?php 
						if(!isset($_SESSION['USERNAME'])){
					?>
					<li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
					<?php
						} else {
					?>
					<li><a href="?logout">Logout</a></li>
					<?php
						}
					?>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Sign In & Sign Up
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<div class="w3_login_module">
							<div class="module form-module">
							  <div class="toggle"><i class="fa fa-times fa-pencil"></i>
								<div class="tooltip">Click Me</div>
							  </div>
							  <div class="form">
								<h3>Login to your account</h3>
								<form action="proseslogin.php" method="post">
								  <input type="text" name="username" placeholder="Username" required="">
								  <input type="password" name="password" placeholder="Password" required="">
								  <input type="submit" value="Login">
								</form>
							  </div>
							  <div class="form">
								<h3>Create an account</h3>
								<form action="register.php" method="post">
								  <input type="text" name="Username" placeholder="Username" required="">
								  <input type="password" name="Password" placeholder="Password" required="">
								  <input type="email" name="Email" placeholder="Email Address" required="">
								  <input type="text" name="Phone" placeholder="Phone Number" required="">
								  <input type="submit" value="Register">
								</form>
							  </div>
							  
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<script>
		$('.toggle').click(function(){
		  // Switches the Icon
		  $(this).children('i').toggleClass('fa-pencil');
		  // Switches the forms  
		  $('.form').animate({
			height: "toggle",
			'padding-top': 'toggle',
			'padding-bottom': 'toggle',
			opacity: "toggle"
		  }, "slow");
		});
	</script>
<!-- //bootstrap-pop-up -->
<!-- nav -->
		<div class="movies_nav">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav>
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Genres <b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns-3">
									<li>
									<div class="col-sm-5">
										<ul class="multi-column-dropdown">
											<?php
											   $queryKategori = mysql_query("SELECT * FROM kategori ORDER BY nama_kategori ASC");
											   while ($data = mysql_fetch_array($queryKategori)){
											?>
											    <li><a href="genres.php?id_genre=<?php echo $data['id_kategori']; ?>"><?php echo $data['nama_kategori'] ?></a></li>
											 <?php
											 	}
											 ?>
										</ul>
									</div>
									<div class="clearfix"></div>
									</li>
								</ul>
							</li>
							<li><a href="list.php">A - z list</a></li>
							<li><a href="contact.php">Contact</a></li>
						</ul>
					</nav>
				</div>
			</nav>	
		</div>
	</div>
<!-- //nav -->
<div class="general_social_icons">
	<nav class="social">
		<ul>
			<li class="w3_twitter"><a href="https://www.twitter.com/JRs_Faisal">Twitter <i class="fa fa-twitter"></i></a></li>
			<li class="w3_facebook"><a href="https://www.facebook.com/jrs.fisalyh">Facebook <i class="fa fa-facebook"></i></a></li>				  
		</ul>
  </nav>
</div>
<!-- single -->
<div class="single-page-agile-main">
<div class="container">
		<!-- /w3l-medile-movies-grids -->
			<div class="agileits-single-top">
				<ol class="breadcrumb">
				  <li><a href="index.html">Home</a></li>
				  <li class="active">Movie</li>
				</ol>
			</div>
			<div class="single-page-agile-info">
                   <!-- /movie-browse-agile -->
                           <div class="show-top-grids-w3lagile">
				<div class="col-sm-8 single-left">
					<div class="song">
						<div class="song-info">
							<h3><?php echo $data2['judul']; ?></h3>	
					</div>
					<br>
						
					<center><img src="images/<?php echo $data2['gambar']; ?>" width='400' class="img-responsive" /></center>
					</div>
					
					<div class="clearfix"> </div>

					<div class="all-comments">
						<div class="all-comments-info">
							
							<div class="agile-info-wthree-box">
												<table>
					<tr>
						<td>Kategori</td>
						<td>:</td>
						<td><?php echo $data2['nama_kategori']; ?></td>
					</tr>
					<tr>
						<td>Produser</td>
						<td>:</td>
						<td><?php echo $data2['produser']; ?></td>
					</tr>
					<tr>
						<td>Produksi</td>
						<td>:</td>
						<td><?php echo $data2['produksi']; ?></td>
					</tr>
					<tr>
						<td>Tanggal Tayang</td>
						<td>:</td>
						<td><?php echo $data2['tanggal_tayang']; ?></td>
					</tr>  
					<tr>
						<td>Durasi</td>
						<td>:</td>
						<td><?php echo $data2['durasi']; ?></td>
					</tr>
				</table>
				<br>
				<p><?php echo $data2['deskripsi']; ?></p>
							</div>
						</div>
						<div class="media-grids">
						</div>
					</div>
				</div>

				<div class="col-md-4 single-right">
					<h3>FILM LAIN</h3>
					<?php
					$filmSamping =  mysql_query("SELECT * FROM movie, kategori WHERE movie.id_kategori = kategori.id_kategori ORDER BY movie.id_movie DESC LIMIT 5");
					while ($queryFilmSamping = mysql_fetch_array($filmSamping)){
					?>
					<div class="single-grid-right">
						<div class="single-right-grids">
							<div class="col-md-4 single-right-grid-left">
								<a href="movie.php?id_movie=<?php echo $queryFilmSamping['id_movie']; ?>"><img src="images/<?php echo $queryFilmSamping['gambar']; ?>" width=150 alt="" /></a>
							</div>
							<div class="col-md-8 single-right-grid-right">
								<a href="movie.php?id_movie=<?php echo $queryFilmSamping['id_movie']; ?>" class="title"> <?php echo $queryFilmSamping['judul']; ?></a>
								<p class="author"><a href="#" class="author"><?php echo $queryFilmSamping['produser']; ?></a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<?php
								}
							?>
				</div>
				

				
				<div class="clearfix"> </div>
			</div>
				<!-- //movie-browse-agile -->
				<!--body wrapper start-->
			
		<!--body wrapper end-->
						
							 
				</div>
				<!-- //w3l-latest-movies-grids -->
			</div>	
		</div>
	<!-- //w3l-medile-movies-grids -->
	
<!-- footer -->
	<div class="footer">
		<div class="container">
				<center><p><font color="#FFFFFF">&copy; 2016 Booking Tiket Bioskop</font></p></center>
			<div class="clearfix"> 
		 </div>
	</div>
<!-- //footer -->
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<!-- //Bootstrap Core JavaScript -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>