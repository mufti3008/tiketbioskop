<?php
function logout(){
	if(isset($_GET["logout"])) {
		$_SESSION["USERNAME"] = "";
		unset($_SESSION["USERNAME"]);
		session_destroy();
		header("location:index.php");
	}
}
?>