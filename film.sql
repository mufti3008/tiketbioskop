-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2017 at 07:48 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `film`
--

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE IF NOT EXISTS `jam` (
  `id_jam` int(11) NOT NULL,
  `jam_tayang` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`id_jam`, `jam_tayang`) VALUES
(5, '18.00');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(2, 'animasi'),
(3, 'Family'),
(4, 'Horor'),
(5, 'Romance'),
(6, 'Action');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi`
--

CREATE TABLE IF NOT EXISTS `konfirmasi` (
  `id_konfirmasi` int(11) NOT NULL,
  `id_pesan` int(11) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_bank_pengirim` varchar(20) NOT NULL,
  `atas_nama_pengirim` varchar(50) NOT NULL,
  `no_rekening_pengirim` varchar(50) NOT NULL,
  `bukti` varchar(50) NOT NULL,
  `status_konfirmasi` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasi`
--

INSERT INTO `konfirmasi` (`id_konfirmasi`, `id_pesan`, `id_rekening`, `id_user`, `nama_bank_pengirim`, `atas_nama_pengirim`, `no_rekening_pengirim`, `bukti`, `status_konfirmasi`) VALUES
(1, 11, 1, 2, 'BRI', 'Faisal', '131312131', '1472464118589.jpg', 1),
(3, 12, 2, 2, 'BRI', 'ica', '1212121', 'FaisalYudoHernawan.jpg', 1),
(4, 13, 1, 7, 'Bangbangtut', 'puti aylin m', '1234', '1472464118589.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE IF NOT EXISTS `movie` (
  `id_movie` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal_tayang` date NOT NULL,
  `produser` varchar(50) NOT NULL,
  `produksi` varchar(50) NOT NULL,
  `durasi` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id_movie`, `id_kategori`, `judul`, `deskripsi`, `tanggal_tayang`, `produser`, `produksi`, `durasi`, `status`, `gambar`) VALUES
(6, 6, 'Transformer 5: The Last Knight', '"Transformers 5: The Last Knight" ini akan menceritakan tentang pemimpin Autobots yaitu Optimus Prime, ia akan menuju ke luar angkas untuk bertemu dengan pencipta bangsanya, Quintessons. Optimus Prime akan melanjutkan misinya sendiri dan bertemu dengan Unicorn untuk mengalahkan Quintessons. Quintessons telah membuat rencana sendiri dan mengirim Lockdown untuk menangkap Optimus Prime, selain itu film ini direncanakan untuk membuat prekuel dan sekuel dari transformers. Sementar itu, Mark Wahlberg didapuk sebagai pemeran Cade Yaegar.\r\n', '2017-06-18', 'Michael Bay', 'Paramount Pictures', '150 Menit', 'New', 'action1.jpg'),
(7, 6, 'World War Z 2', 'Film "World War Z 2" akan menceritakan tentang alur yang hampir sama dengan film sebelumnya, yakni tentang serangan dari bangsa zombie yang mengganggu kota. Dimana semua orang arus berperang melawan Zombie yang berkeliaran. Film ini merupakan sekuel dari film blockbuster, Bradpitt akan kembali dalam perannya sebagai pegawai PBB bernama Gerry Lane. Sementara itu, Marc Forster tidak lagi mengarahkan sekuel, dia akan digantikan oleh Juan Antonio Bayona.', '2017-06-09', 'Bradd Pitt', 'Skydance Productions', '165 Menit', 'New', 'action2.jpg'),
(8, 2, 'The Boss Baby', 'The Boss Baby mengisahkan tentang anak berusia 7 tahun yang iri dengan adiknya yang masih bayi karna kasih sayang kedua orang tuanya yang lebih mengutamakan adiknya.', '2017-08-01', 'Denise Nolan Cascino, Ramsey Ann Naito', 'Dream Works', '175 Menit', 'Coming Soon', 'animasi1.jpg'),
(9, 2, 'The Croods 2', 'The Croods 2 adalah sebuah film animasi terbaru Hollywood yang direncanakan rilis pada tanggal 5 January 2018 (USA). Film ini memiliki perpaduan dari beberapa genre yang menarik yaitu Animation, Adventure, dan Comedy. Film Animation Hollywood ini adalah merupakan lanjutan dari film sebelumnya yang telah dirilis pada tahun 2013 yang telah silam. Untuk dibeberapa negara film ini memiliki nama judul yang berbeda, yaitu diantaranya seperti di negara Brazil berjudul "Os Croods 2", di negara Germany berjudul "Croods 2", di negara France berjudul "Les Croods 2", di negara Greece berjudul "ÎŸÎ¹ ÎšÏÎ¿Ï…Î½Ï„Ï‚ 2",di negara Poland berjudul "Krudowie 2", dan di negara Russia berjudul "Ð¡ÐµÐ¼ÐµÐ¹ÐºÐ° ÐšÑ€ÑƒÐ´Ñ 2".', '2018-01-02', 'Kirk De Micco', 'Dream Works', '18o Menit', 'Coming Soon', 'animasi2.jpg'),
(10, 3, 'Surga Yang Tak di Rindukan 2', 'Meirose (Raline Shah) datang kembali ke rumah Pras (Fedi Nuril) dan Arini (Laudya Cynthia Bella). Kedatangannya bermaksud untuk mengambil Akbar, putranya yang selama ini dirawat oleh pasangan suami istri tersebut. Namun kali ini Arini membujuk dan meminta Meirose untuk kembali kepada Pras.\r\n\r\nMeirose pun dalam kebimbangan. Ia kini ragu dengan pilihan hidupnya antara menjalani masa depan yang ia sendiri tak tahu seperti apa jadinya, atau kembali pada Pras namun tak sampai hati merusak kebahagiaan Arini. Meirose makin bingung saat Pras, pria yang masih dicintainya muncul di depannya.\r\n\r\nMengapa Arini bersikeras membujuk Meirose untuk kembali pada Pras? Apa yang dilakukan Pras, apakah ia akan menerima Meirose, sedangkan ia masih ragu dengan kemampuannya sebagai manusia biasa untuk berlaku adil pada 2 istri.', '2017-01-20', 'Manoj Punjabi', 'MD Picture', '190 Menit', 'New', 'family1.jpg'),
(11, 3, 'Kartini', 'Kartini adalah sebuah film biografi dari tokoh perjuangan emansipasi wanita Indonesia Kartini. Film ini menjadi penampilan ketiga Kartini di layar lebar setelah biografi R.A. Kartini (film) (1984), dan kisah fiksi asmara Kartini Surat Cinta Untuk Kartini (2016). Dian Sastrowardoyo akan berperan sebagai Kartini.', '2017-04-20', 'Hanung Bramantyo', 'Legacy Pictures, Screenplay Films', '195 Menit', 'New', 'family2.PNG'),
(12, 4, 'Rings', 'Cerita â€œRingsâ€ berawal saat Julia yang merupakan siswi SMA berpacaran jarak jauh dengan seorang mahasiswa bernama Holt. Khawatir dengan hubungan mereka, Julia pergi untuk melihat pacarnya tersebut dan mengetahui bahwa kekasihnya itu merupakan bagian dari klub mahasiswa yang melewati rekaman Samara.  Julia mengetahui bahwa Holt telah menonton rekaman terkutuk tersebut enam setengah hari yang lalu, yang berarti bahwa hari ini kekasihnya tersebut akan mati. Kutukan itu membawa mereka kembali bersama, tapi juga akan mengancam nyawa mereka.', '2017-02-02', 'Laurie MacDonald', 'Paramount Pictures', '199 Menit', 'New', 'horror1.jpg'),
(13, 5, 'Fifty Shades Darker', 'Fifty Shades Darker adalah film erotis romantis Inggris-Amerika yang akan segera tayang disutradarai oleh James Foley dan ditulis oleh Niall Leonard, berdasarkan novel Fifty Shades Darker oleh penulis Inggris E. L. James. Ini adalah film kedua dalam seri film Fifty Shades, dan sekuel dari film 2015 Fifty Shades of Grey. Film ini dibintangi oleh Dakota Johnson dan Jamie Dornan sebagai Anastasia Steele dan Christian Grey.', '2017-02-09', 'Dana Brunetti', 'Paramount Pictures', '199 Menit', 'New', 'romance1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id_pesan` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  `id_studio` int(11) NOT NULL,
  `id_jam` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `no_kursi` varchar(5) NOT NULL,
  `harga` int(11) NOT NULL,
  `status_pesan` enum('Terbayar','Belum Terbayar') NOT NULL DEFAULT 'Belum Terbayar'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `id_movie`, `id_studio`, `id_jam`, `id_user`, `id_rekening`, `tanggal`, `no_kursi`, `harga`, `status_pesan`) VALUES
(11, 6, 4, 5, 2, 1, '2016-12-28', 'A1', 25000, 'Terbayar'),
(12, 6, 4, 5, 2, 2, '2016-12-30', 'A1', 25000, 'Terbayar'),
(13, 6, 4, 5, 7, 1, '2016-12-31', 'A1', 25000, 'Terbayar');

-- --------------------------------------------------------

--
-- Table structure for table `rekening`
--

CREATE TABLE IF NOT EXISTS `rekening` (
  `id_rekening` int(11) NOT NULL,
  `nama_bank` varchar(20) NOT NULL,
  `atas_nama` varchar(50) NOT NULL,
  `no_rekening` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekening`
--

INSERT INTO `rekening` (`id_rekening`, `nama_bank`, `atas_nama`, `no_rekening`) VALUES
(1, 'BANK MANDIRI', 'Faisal', '09389834893849343'),
(2, 'BANK BRI', 'Putri', '32948923483234');

-- --------------------------------------------------------

--
-- Table structure for table `studio`
--

CREATE TABLE IF NOT EXISTS `studio` (
  `id_studio` int(11) NOT NULL,
  `nama_studio` varchar(50) NOT NULL,
  `alamat_studio` varchar(50) NOT NULL,
  `No_Telepon` varchar(12) NOT NULL,
  `kursi` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studio`
--

INSERT INTO `studio` (`id_studio`, `nama_studio`, `alamat_studio`, `No_Telepon`, `kursi`) VALUES
(4, 'Studio 2', 'Sumampir', '0822222222', 150);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL,
  `user_role` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `no_hp` varchar(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `user_role`, `username`, `password`, `email`, `no_hp`) VALUES
(1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'faisalyudo@gmail.com', '082242698711'),
(2, 1, 'faisal', 'f4668288fdbf9773dd9779d412942905', 'faisalyudo@gmail.com', '082242698711'),
(3, 1, 'yudo', '87980d89dd953a0b18edd3d053ec6a18', 'faisalyudo@gmail.com', '082242698711'),
(4, 1, 'keket', '7c415eebfee09343a497e3e1b6594d20', 'faisal@gmail.com', '08888'),
(5, 1, 'dono', 'e3b810115555736a216f137df55789f6', 'faisal@gmail.com', '08888'),
(6, 1, 'putriayln', '4d2606237ea94965b5405c99863da39a', 'putriaylin@gmail.com', '081282448804'),
(7, 1, 'uticantik', '4d2606237ea94965b5405c99863da39a', 'putriaylin@gmail.com', '081282448804');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jam`
--
ALTER TABLE `jam`
  ADD PRIMARY KEY (`id_jam`) USING BTREE;

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`) USING BTREE;

--
-- Indexes for table `konfirmasi`
--
ALTER TABLE `konfirmasi`
  ADD PRIMARY KEY (`id_konfirmasi`) USING BTREE;

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id_movie`) USING BTREE;

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`) USING BTREE;

--
-- Indexes for table `rekening`
--
ALTER TABLE `rekening`
  ADD PRIMARY KEY (`id_rekening`) USING BTREE;

--
-- Indexes for table `studio`
--
ALTER TABLE `studio`
  ADD PRIMARY KEY (`id_studio`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jam`
--
ALTER TABLE `jam`
  MODIFY `id_jam` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `konfirmasi`
--
ALTER TABLE `konfirmasi`
  MODIFY `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id_movie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `rekening`
--
ALTER TABLE `rekening`
  MODIFY `id_rekening` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `studio`
--
ALTER TABLE `studio`
  MODIFY `id_studio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
