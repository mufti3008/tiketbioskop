<?php
function logout(){
	if(isset($_GET["logout"])) {
		$_SESSION["USERNAME"] = "";
		unset($_SESSION["USERNAME"]);
		session_destroy();
	}
}
function cekuser(){
	if(!isset($_SESSION["USERNAME"])){
		header("location:index.php");
		exit;
	}
}
?>