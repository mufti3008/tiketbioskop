<!DOCTYPE html>
<?php
    session_start();
    require_once("../config/config.php");
    if(!isset($_SESSION['USERNAME'])){
        header("location:index.php");
     } elseif($_SESSION['ROLE'] != 0) {
        header("location:../login/");
    } else {
?>
<?php
    require_once("logout.php");
    logout();
    cekuser();
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bootstrap/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bootstrap/ionicons/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-yellow sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="../index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo "Admin Panel"; ?></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['USERNAME'] ;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['USERNAME'] ;?>
                    </p>
                  </li>
                
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="changePassword.php" class="btn btn-default btn-flat"><i class="fa fa-lock"></i> Change Password</a>  
                    </div>
                    <div class="pull-right">
                      <a href="?logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['USERNAME'] ;?></p>
            </div>
          </div>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
           <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
             <li>
              <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href='dataUser.php'>";
                echo "<i class='fa fa-user'></i> <span>User</span>";
                echo "</a>";
                echo "<li>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-video-camera'></i>";
                echo "    <span>Studio</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahStudio.php'><i class='fa fa-pencil'></i> Tambah Studio</a></li>";
                echo "    <li><a href='dataStudio.php'><i class='fa fa-table'></i> Data Studio</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-clock-o'></i>";
                echo "    <span>Jam Tayang</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahJam.php'><i class='fa fa-pencil'></i> Tambah Jam Tayang</a></li>";
                echo "    <li><a href='dataJam.php'><i class='fa fa-table'></i> Data Jam Tayang</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
               ?>
              <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-file-movie-o'></i>";
                echo "    <span>Kategori</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahKategori.php'><i class='fa fa-pencil'></i> Tambah Kategori</a></li>";
                echo "    <li><a href='dataKategori.php'><i class='fa fa-table'></i> Data Kategori</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
              ?>
              <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-film'></i>";
                echo "    <span>Film</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahFilm.php'><i class='fa fa-pencil'></i> Tambah Film</a></li>";
                echo "    <li><a href='dataFilm.php'><i class='fa fa-table'></i> Data Film</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 1) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-calendar'></i>";
                echo "    <span>Booking</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='booking.php'><i class='fa fa-pencil'></i> Booking Tiket</a></li>";
                echo "    <li><a href='riwayatBooking.php'><i class='fa fa-table'></i> Riwayat</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='active'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-clipboard'></i>";
                echo "    <span>Laporan</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li class='active'><a href='dataOrder.php'><i class='fa fa-shopping-cart'></i> Data Order</a></li>";
                echo "    <li><a href='dataKonfirmasi.php'><i class='fa fa-money'></i> Data Konfirmasi</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Order
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Laporan</a></li>
            <li class="active">Data Order</li>
          </ol>
        </section>

       <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                <div class="table-responsive">
                  <table id="table1" class="table table-bordered table-striped">
                  <?php
                     if(@$_GET["booking"] == "sukses"){
                  ?>
            <div class="alert fade in alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Berhasil booking !</strong>
                </div>
            <?php
            } else if(@$_GET["konfirmasi"] == "sukses") {
              ?>
              <div class="alert fade in alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Konfirmasi pembayaran berhasil dikirimkan !</strong>
                </div>
            <?php
             } else if(@$_GET["konfirmasi"] == "gagal") {
              ?>
              <div class="alert fade in alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Konfirmasi pembayaran gagal dikirimkan</strong>
                </div>
            <?php
            }
            ?>
                    <thead>
                      <tr class="info">
                        <th><center>Booking Code</center></th>
                        <th><center>Nama Pemesan</center></th>
                        <th><center>Film</center></th>     
                        <th><center>Studio</center></th>
                        <th><center>Tanggal</center></th>
                        <th><center>Jam</center></th>
                        <th><center>Harga</center></th>
                        <th><center>BANK Tujuan</center></th>
                        <th><center>Atas Nama Tujuan</center>
                        <th><center>No Rekening Tujuan</center>
                        <th><center>Status</center></th>
                      
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no=1;
                        
                        $sql = "SELECT * FROM user, pesan, movie, studio, jam, rekening WHERE pesan.id_movie = movie.id_movie AND pesan.id_studio = studio.id_studio AND pesan.id_jam = jam.id_jam AND pesan.id_rekening = rekening.id_rekening AND pesan.id_user = user.id_user";
                        $query = mysql_query($sql);
                        while ($data = mysql_fetch_array($query)) {
                    ?>
                      <tr>
                         <td><center><?php echo $data['id_pesan'] ?></center></td>
                         <td><?php echo $data['username'] ?></td>
                         <td><center><?php echo $data['judul']; ?></center></td>      
                         <td><center><?php echo $data['nama_studio']; ?></center></td> 
                         <td><center><?php echo $data['tanggal']; ?></center></td>
                         <td><center><?php echo $data['jam_tayang']; ?></center></td>
                         <td><center><?php echo $data['harga']; ?></center></td>
                         <td><center><?php echo $data['nama_bank']; ?></center></td>
                         <td><center><?php echo $data['atas_nama']; ?></center></td>
                         <td><center><?php echo $data['no_rekening']; ?></center></td>
                         <td><center><?php echo $data['status_pesan']; ?></center></td>
                        
                      </tr>
                      <?php
                          $no++;
                              }
                       ?>   
                    </tbody>
                  </table>
                  </div>
                </div><!-- /.box-body -->
                <style type="text/css">
                  .table-responsive { overflow-x: initial; }
                </style>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2016<a href="<?php echo $url; ?>"> <?php echo $namaCafe; ?></a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- page script -->
     <script src="../plugins/js/jquery-1.11.0.js"></script>
    <script type="text/javascript">
            $(function() {
                $("#table1").dataTable();
            });
        </script>

  </body>
  <?php
};
?>
</html>
