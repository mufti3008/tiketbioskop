<!DOCTYPE html>
<?php
    session_start();
    require_once("../config/config.php");
    if(!isset($_SESSION['USERNAME'])){
        header("location:../index.php");
     }else{
?>
<?php
    require_once("logout.php");
    logout();
    cekuser();
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bootstrap/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bootstrap/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="../https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-yellow sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo "Admin Panel" ?></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['USERNAME'] ;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                       <p><?php echo $_SESSION['USERNAME'] ;?></p>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                  <div class="pull-left">
                      <a href="changePassword.php" class="btn btn-default btn-flat"><i class="fa fa-lock"></i> Change Password</a>  
                    </div>
                    <div class="pull-right">
                      <a href="?logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['USERNAME'] ;?></p>
              
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href='dataUser.php'>";
                echo "<i class='fa fa-user'></i> <span>User</span>";
                echo "</a>";
                echo "<li>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-video-camera'></i>";
                echo "    <span>Studio</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahStudio.php'><i class='fa fa-pencil'></i> Tambah Studio</a></li>";
                echo "    <li><a href='dataStudio.php'><i class='fa fa-table'></i> Data Studio</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-clock-o'></i>";
                echo "    <span>Jam Tayang</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahJam.php'><i class='fa fa-pencil'></i> Tambah Jam Tayang</a></li>";
                echo "    <li><a href='dataJam.php'><i class='fa fa-table'></i> Data Jam Tayang</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
               ?>
              <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-file-movie-o'></i>";
                echo "    <span>Kategori</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='tambahKategori.php'><i class='fa fa-pencil'></i> Tambah Kategori</a></li>";
                echo "    <li><a href='dataKategori.php'><i class='fa fa-table'></i> Data Kategori</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
              ?>
              <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='active'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-film'></i>";
                echo "    <span>Film</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li class='active'><a href='tambahFilm.php'><i class='fa fa-pencil'></i> Tambah Film</a></li>";
                echo "    <li><a href='dataFilm.php'><i class='fa fa-table'></i> Data Film</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
            <?php
              if($_SESSION['ROLE'] == 0) {
                echo "<li class='treeview'>";
                echo "<a href=''>";
                echo "   <i class='fa fa-clipboard'></i>";
                echo "    <span>Laporan</span><i class='fa fa-angle-left pull-right'></i></a>";
                echo "    <ul class='treeview-menu'>";
                echo "   <li><a href='dataOrder.php'><i class='fa fa-shopping-cart'></i> Data Order</a></li>";
                echo "    <li><a href='dataKonfirmasi.php'><i class='fa fa-money'></i> Data Konfirmasi</a></li>";
                echo "  </ul>";
                echo "</li>";
              }
            ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Film
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Film</a></li>
            <li class="active">Tambah Film</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                <?php
                     if(@$_GET["film"] == "sukses"){
                ?>
                    <div class="alert fade in alert-success">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Film berhasil ditambahkan !</strong>
                      </div>
                <?php
                    } else if(@$_GET["film"] == "gagal"){
                ?>
                    <div class="alert fade in alert-error">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Film gagal ditambahkan !</strong>
                    </div>
                <?php
                   } else if(@$_GET["film"] == "kosong"){
                ?>
                 <div class="alert fade in alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Film tidak boleh kosong !</strong>
                    </div>
                <?php
                  }
                ?>
               
                <form class="form-horizontal" enctype="multipart/form-data" action="prosesTambahFilm.php" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Judul Film</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="judulFilm" placeholder="Judul Film" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Kategori</label>
                      <div class="col-sm-6">
                        <select name="idKategori" class="form-control">
                          <?php
                            $query = mysql_query("SELECT * FROM kategori");
                              while($dataArray = mysql_fetch_array($query)){
                                echo "<option value='".$dataArray['id_kategori']."'>".$dataArray['nama_kategori']."</option>";
                            }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Deskripsi</label>
                      <div class="col-sm-6">
                        <textarea class="form-control" name="deskripsiFilm" rows="10"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Tanggal Tayang</label>
                      <div class="col-sm-6">
                        <input type="date" class="form-control" name="tanggalTayang" placeholder="Tanggal Tayang" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Produser</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="produserFilm" placeholder="Produser" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Produksi</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="produksi" placeholder="Produksi" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Durasi</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="durasiFilm" placeholder="Durasi" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Status</label>
                      <div class="col-sm-6">
                        <select name="statusFilm" class="form-control">
                            <option value="Coming Soon">Coming Soon</option>
                            <option value="New">New</option>
                            <option value="Old">Old</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-6">
                          <input type="file" name="file">
                        </div>
                    </div>
                    <center><button type="submit" class="btn btn-warning btn-lg">Simpan</button></center> 
                 
                </form>
                </div><!-- /.row -->
          
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2016<a href="<?php echo $url; ?>"> <?php echo $namaCafe; ?></a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="../plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="../https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
  </body>
  <?php
}
?>
</html>